﻿# Assignment 3 - Web API Creattion in ASP.NET Core

Third assignment in Noroff .NET course. Web Server Development Module

## Description

### Part one - Entity Framework Core Code First
In the first part of this assignment we made a database using EF Core with the code first approach. This included tables for Movies, Characters and Franchises (+ a relational table for the
movie and characters relationship). We also included some seeding data for testing purposes.

### Part two - Web API using ASP.NET Core
In this part of the assignment we created generic CRUD operations for the tables created in part one. In addition to the generic CRUD operations there are put methods for updating
characters in a movie and updating movies in a franchise. Datatransfer objects where also created for the different CRUD operations and for specific reads like getting all movies in a franchise
and getting characters in a movie. These DTO's where made using Automapper. Also included in this part was documentation with swagger and some services for making the specific reads and updates
possible. The services also helped clean up the controllers where it was needed. Lastly a CI pipeline was made in gitlab to create the Docker image of this API also included in this repository
under Container Registry.

## Project status
Most of the requierments are completed. The function that gets all the characters in a franchise returns the wrong values.

## Usage
This assignment is run via the Docker image or via Visual Studio.

## Technologies
* C#
* .NET
* EF Core
* ASP.NET 
* Automapper
* Microsoft SQL Server Management Studio

## Db Diagram - Movie Characters Db
A diagram of the database that is created by using EF Core code first approach
![dbDiagram](https://gitlab.com/AnneRike/assignment3/uploads/bd048c79fd4d8354689874ec7dd8073a/dbDiagramAss3.png)

## Contributers
* Anne Rike
* Erik Aardal

### License
[MIT](https://choosealicense.com/licenses/mit/)
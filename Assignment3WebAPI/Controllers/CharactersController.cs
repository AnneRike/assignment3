﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3WebAPI.Data;
using Assignment3WebAPI.Model;
using AutoMapper;
using Assignment3WebAPI.DataTransferObjects.CharacterDTO;

namespace Assignment3WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        // GET: api/Characters
        /// <summary>
        /// Get all characters from the database.
        /// </summary>
        /// <returns>A mapped list with all the characters and a responsetype indicating success.</returns>
        /// <response code="200">Successfully got all characters from the database (OK)</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.ToListAsync());
        }


        // GET: api/Characters/5
        /// <summary>
        /// Get one specific character from the database.
        /// </summary>
        /// <param name="id">Id for the specific character</param>
        /// <returns>Mapped Character object</returns>
        /// <response code="200">Found the character by the id and returned the mapped character object (OK).</response>
        /// <response code="404">The character is not found (Not found).</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the values of an existing character.
        /// </summary>
        /// <param name="id">The id of the specific character that should be updated.</param>
        /// <param name="characterDto">The character object with updated information.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <response code="204">The update was successful (No content)</response>
        /// <response code="400">The id does not match the id of the character (Bad request)</response>
        /// <response code="404">The character with the specified id does not exist (Not found)</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDto)
        {
            if (id != characterDto.Id)
            {
                return BadRequest();
            }

            if (!CharacterExists(id))
            {
                return NotFound();
            }

            Character character = _mapper.Map<Character>(characterDto);
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a character to the database.
        /// </summary>
        /// <param name="characterDto">The character that should be added.</param>
        /// <returns>The created character.</returns>
        /// <response code="201">The new character was successfully created (Created)</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterDto)
        {
            var characterModel = _mapper.Map<Character>(characterDto);
            _context.Characters.Add(characterModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBook", new { id = characterModel.Id }, _mapper.Map<Character>(characterModel));
        }
        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes a specific character from the database by id.
        /// </summary>
        /// <param name="id">The id of the specific character.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <response code="204">The deletion was successful (No content)</response>
        /// <response code="404">The given id could not be found (Not found)</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)] 
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Checks if the character exists.
        /// </summary>
        /// <param name="id">The id of the character.</param>
        /// <returns>A boolean indicating if a character with the specified id exists in the database.</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}

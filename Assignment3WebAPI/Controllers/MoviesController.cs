﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3WebAPI.Data;
using Assignment3WebAPI.Model;
using AutoMapper;
using Assignment3WebAPI.DataTransferObjects.MovieDTO;
using Assignment3WebAPI.Service;
using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;

namespace Assignment3WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }
        // GET: api/Movies
        /// <summary>
        /// Get all movies from the database. 
        /// </summary>
        /// <returns>A mapped list of all the movies and a responsetype indicating success.</returns>
        /// <response code="200">Successfully got all movies from the database (OK)</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }
        // GET: api/Movies/5
        /// <summary>
        /// Get a movie from the database by id.
        /// </summary>
        /// <param name="id">Id of movie to get.</param>
        /// <returns>Mapped movie object.</returns>
        /// <response code="200">Found the movie by the id and returned the mapped movie object (OK).</response>
        /// <response code="404">The movie is not found (Not found).</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }
        // GET: api/Movies
        /// <summary>
        /// Get all characters in a specific Movie by id.
        /// </summary>
        /// <param name="id">The id of the movie to get the characters from.</param>
        /// <returns>A list of characters.</returns>
        /// <response code="200">Found the movie by the id and returned the list (OK).</response>
        /// <response code="404">The movie is not found (Not found).</response>
        [HttpGet("getCharacters/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<MovieCharactersDTO>>> GetCharactersInMovie(int id)
        {
            List<Movie> movie = await _movieService.GetSpecificAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            var movieRead = _mapper.Map<List<MovieCharactersDTO>>(movie);
            return movieRead;
        }
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the value of an existing movie. 
        /// </summary>
        /// <param name="id">The id of the movie that should be updated.</param>
        /// <param name="movieDto">The movie object with updated information.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <response code="204">The update was successful (No content)</response>
        /// <response code="400">The id does not match the id of the movie (Bad request)</response>
        /// <response code="404">The movie with the specified id does not exist (Not found)</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDto)
        {
            if (id != movieDto.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie movie = _mapper.Map<Movie>(movieDto);
            await _movieService.UpdateMovieAsync(movie);

            return NoContent();

        }
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a movie to the database.
        /// </summary>
        /// <param name="movieDto">The movie that should be added.</param>
        /// <returns>The created movie.</returns>
        /// /// <response code="201">The new movie was successfully created (Created)</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieDto)
        {
            var movieModel = _mapper.Map<Movie>(movieDto);
            movieModel = await _movieService.AddMovieAsync(movieModel);

            return CreatedAtAction("GetMovie",
                new { id = movieModel.Id },
                _mapper.Map<MovieReadDTO>(movieModel));

        }
        // DELETE: api/Movies/5
        /// <summary>
        /// Deletes a movie from the database by id.
        /// </summary>
        /// <param name="id">The id of the movie.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <response code="204">The deletion was successful (No content)</response>
        /// <response code="404">The requested movie could not be found (Not found)</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();

        }
        /// <summary>
        /// Updates the characters in a movie.
        /// </summary>
        /// <param name="id">The id of the movie.</param>
        /// <param name="characters">A list of character ids.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <reponse code="204">The update was successful. The characters in the movie were updated (No content)</reponse>
        /// <reponse code="400">Invalid character in the characters list(Bad request)</reponse>
        /// <reponse code="404">The movie with the given id doesn't exist (Not Found)</reponse>
        [HttpPut("{id}/characters")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character.");
            }

            return NoContent();
        }
    }
}

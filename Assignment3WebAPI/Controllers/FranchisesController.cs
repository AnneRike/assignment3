﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3WebAPI.Data;
using Assignment3WebAPI.Model;
using Assignment3WebAPI.Service;
using AutoMapper;
using Assignment3WebAPI.DataTransferObjects.MovieDTO;
using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;

namespace Assignment3WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")]
    [Consumes("Application/json")]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }
        // GET: api/Franchises
        /// <summary>
        /// Get all franchises from the database. 
        /// </summary>
        /// <returns>A mapped list with all the franchises and a responsetype indicating success.</returns>
        /// <response code="200">Successfully got all franchises from the database (OK)</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }
        // GET: api/Franchises/5
        /// <summary>
        /// Get a franchise from the database by id.
        /// </summary>
        /// <param name="id">Id of franchise to get.</param>
        /// <returns>Mapped franchise object.</returns>
        /// <response code="200">Found the franchise by the id and returned the mapped franchise object (OK).</response>
        /// <response code="404">The franchise is not found (Not found).</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchise);
        }
        // GET: api/Franchises
        /// <summary>
        /// Get all movies in a specific Franchise.
        /// </summary>
        /// <param name="id">The id of the franchise to get the movies from.</param>
        /// <returns>A list of movies</returns>
        /// <response code="200">Found the franchise by the id and returned the list (OK).</response>
        /// <response code="404">The franchise is not found (Not found).</response>
        [HttpGet("getMovies/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<FranchiseMoviesDTO>>> GetMoviesInFranchise(int id)
        {
            List<Franchise> franchise = await _franchiseService.GetSpecificAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }
            var franchiseRead = _mapper.Map<List<FranchiseMoviesDTO>>(franchise);
            return franchiseRead;
        }
        // GET: api/Franchises
        /// <summary>
        /// Get all characters in a specific Franchise.
        /// </summary>
        /// <param name="id">The id of the franchise to get the characters from.</param>
        /// <returns>A list of characters.</returns>
        /// <response code="200">Found the franchise by the id and returned the list (OK).</response>
        /// <response code="404">The franchise is not found (Not found).</response>
        [HttpGet("getCharacters/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<FranchiseCharactersDTO>>> GetCharactersInFranchise(int id)
        {
            List<Character> franchise = await _franchiseService.GetCharactersAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }
            var franchiseRead = _mapper.Map<List<FranchiseCharactersDTO>>(franchise);
            return franchiseRead;
        }
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates a franchise in the database.
        /// </summary>
        /// <param name="id">The id of the franchise that should be updated.</param>
        /// <param name="franchiseDto">The franchise object with updated information.</param>
        /// <returns>A responsetype indicating success or whether the character was found.</returns>
        /// <response code="204">The update was successful (No content)</response>
        /// <response code="400">The id does not match the id of the franchise (Bad request)</response>
        /// <response code="404">The franchise with the specified id does not exist (Not found)</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDto)
        {
            if (id != franchiseDto.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.UpdateFranchiseAsync(franchise);

            return NoContent();
        }
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Adds a franchise to the database.
        /// </summary>
        /// <param name="franchiseDto">The franchise that should be added.</param>
        /// <returns>The created franchise.</returns>
        /// <response code="201">The new franchise was successfully created (Created)</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            var franchiseModel = _mapper.Map<Franchise>(franchiseDto);
            franchiseModel = await _franchiseService.AddFranchiseAsync(franchiseModel);

            return CreatedAtAction("GetFranchise",
                new { id = franchiseModel.Id },
                _mapper.Map<FranchiseReadDTO>(franchiseModel));
        }
        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes a franchise from the database by id. 
        /// </summary>
        /// <param name="id">The id of the franchise.</param>
        /// <returns>A responsetype indicating success and whether the franchise was found.</returns>
        /// <produce code="204">The deletion was successful (No content)</produce>
        /// <produce code="404">The given id could not be found (Not found)</produce>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Updates the movies in a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise.</param>
        /// <param name="movies">A list of movie ids.</param>
        /// <returns>A responsetype indicating success or whether the franchise was found.</returns>
        /// <reponse code="204">The update was successful. The movies in the franchise were updated (No content)</reponse>
        /// <reponse code="400">Invalid movie in the movies list (Bad request)</reponse>
        /// <reponse code="404">The franchise with the given id doesn't exist (Not Found)</reponse>
        [HttpPut("{id}/movies")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateMovieFranchisesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie.");
            }

            return NoContent();
        }
    }
}

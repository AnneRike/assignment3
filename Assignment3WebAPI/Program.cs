using Assignment3WebAPI.Data;
using Assignment3WebAPI.Service;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace Assignment3WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            /* --- Code for the noroff assignment --- */

            // Adds the connectionstring 
            builder.Services.AddDbContext<MovieCharactersDbContext>(Option => Option.UseSqlServer(builder.Configuration.GetConnectionString("MovieCharactersConnection")));
           
            // Adds automapper
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // Adds custom services 
            builder.Services.AddScoped<IMovieService, MovieService>();
            builder.Services.AddScoped<IFranchiseService, FranchiseService>();

            // Swagger documentations
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Movie Characters API",
                    Description = "An ASP.NET Core Web API for managing Movies, Characters and Franchises",
                    Contact = new OpenApiContact
                    {
                        Name = "Erik Aardal and Anne Rike",
                        Email = "erik.aardal@no.experis.com, anne.rike@no.experis.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT License",
                        Url = new Uri("https://choosealicense.com/licenses/mit/")
                    }
                });
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });
            /*-------------------------------------------------------------------------------------*/

            /* Code from ASP.NET Web API template */

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
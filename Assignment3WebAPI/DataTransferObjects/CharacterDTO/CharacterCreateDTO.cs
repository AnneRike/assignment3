﻿using Assignment3WebAPI.Model;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Assignment3WebAPI.DataTransferObjects.CharacterDTO
{
    /// <summary>
    /// Datatransfer object for creating a character
    /// </summary>
    public class CharacterCreateDTO
    {
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Picture { get; set; }
    }
}

﻿namespace Assignment3WebAPI.DataTransferObjects.CharacterDTO
{
    /// <summary>
    /// Datatransfer object for reading a character
    /// </summary>
    public class CharacterReadDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string? Picture { get; set; }
    }
}

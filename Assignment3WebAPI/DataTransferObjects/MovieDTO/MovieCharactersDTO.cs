﻿namespace Assignment3WebAPI.DataTransferObjects.MovieDTO
{
    /// <summary>
    /// Datatransfer object for getting Characters in a Movie
    /// </summary>
    public class MovieCharactersDTO
    {
        public List<int> Characters { get; set; }
    }
}

﻿using Assignment3WebAPI.Model;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Assignment3WebAPI.DataTransferObjects.MovieDTO
{
    /// <summary>
    /// Datatransfer object for creating a Movie
    /// </summary>
    public class MovieCreateDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
    }
}

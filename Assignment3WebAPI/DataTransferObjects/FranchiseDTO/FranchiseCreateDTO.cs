﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3WebAPI.DataTransferObjects.FranchiseDTO
{
    /// <summary>
    /// Datatransfer object for creating a Franchise
    /// </summary>
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

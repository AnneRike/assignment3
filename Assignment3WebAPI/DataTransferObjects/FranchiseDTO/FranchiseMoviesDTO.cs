﻿namespace Assignment3WebAPI.DataTransferObjects.FranchiseDTO
{
    /// <summary>
    /// Datatransfer object for getting Movies in a Franchise
    /// </summary>
    public class FranchiseMoviesDTO
    {
        public List<int>? Movies { get; set; }
    }
}

﻿namespace Assignment3WebAPI.DataTransferObjects.FranchiseDTO
{
    /// <summary>
    /// Datatransfer object for reading a Franchise
    /// </summary>
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int>? Movies { get; set; }
    }
}

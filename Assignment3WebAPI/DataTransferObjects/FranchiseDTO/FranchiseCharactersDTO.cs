﻿namespace Assignment3WebAPI.DataTransferObjects.FranchiseDTO
{
    /// <summary>
    /// Datatransfer object for getting Characters in a Franchise
    /// </summary>
    public class FranchiseCharactersDTO
    {
        public List<int> Characters { get; set; }
    }
}

﻿namespace Assignment3WebAPI.DataTransferObjects.FranchiseDTO
{
    /// <summary>
    /// Datatransfer object for editing a Franchise
    /// </summary>
    public class FranchiseEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}


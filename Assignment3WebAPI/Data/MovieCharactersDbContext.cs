﻿using Assignment3WebAPI.Model;
using Microsoft.EntityFrameworkCore;
using NuGet.DependencyResolver;
using System.IO;

namespace Assignment3WebAPI.Data
{
    public class MovieCharactersDbContext : DbContext
    {
        public MovieCharactersDbContext(DbContextOptions options) : base(options)
        {
        }
        /// <summary>
        /// Creates seeding data
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //seeding 3 movies to the database for testing purposes
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    Title = "El Camino",
                    Genre = "Drama, Drugs",
                    ReleaseYear = 2020,
                    Director = "Erik Aardal",
                    Picture = "https://picsum.photos/200/300",
                    Trailer = "https://www.youtube.com/watch?v=C0DPdy98e4c",
                    FranchiseId = 1
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    Title = "Avangers",
                    Genre = "Drama, Superhero, Action",
                    ReleaseYear = 2013,
                    Director = "Jarand Larsen",
                    Picture = "https://picsum.photos/200/300",
                    Trailer = "https://www.youtube.com/watch?v=C0DPdy98e4c",
                    FranchiseId = 2
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    Title = "Avangers Endgame",
                    Genre = "Drama, Superhero, Action",
                    ReleaseYear = 2020,
                    Director = "Jarand Larsen",
                    Picture = "https://picsum.photos/200/300",
                    Trailer = "https://www.youtube.com/watch?v=C0DPdy98e4c",
                    FranchiseId = 2
                });
            //Seeding Characters to database for testing purposes
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 1,
                    FullName = "Jesse Pinkman",
                    Alias = "The capn",
                    Gender = "Male",
                    Picture = "https://picsum.photos/200/300",
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id= 2,
                    FullName = "Tony Stark",
                    Alias = "Iron Man",
                    Gender = "Male",
                    Picture = "https://picsum.photos/200/300",
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id= 3,
                    FullName = "Thor",
                    Alias = "God of Thunder",
                    Gender = "Male",
                    Picture = "https://picsum.photos/200/300",
                });

            //Seeding Franchises to the database for testing purposes
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 1,
                    Name = "Breaking Bad",
                    Description = "Breaking Bad universe",
                });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "MCU",
                    Description = "Marvel Cinematic Universe",
                });
            //Many to many seeding
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    je =>
                    {
                        je.HasKey("MoviesId", "CharactersId");
                        je.HasData(
                            new { MoviesId = 1, CharactersId = 1 },
                            new { MoviesId = 2, CharactersId = 2 },
                            new { MoviesId = 2, CharactersId = 3 },
                            new { MoviesId = 3, CharactersId = 2 },
                            new { MoviesId = 3, CharactersId = 3 }
                        );
                    });
            /*
            modelBuilder.Entity<Movie>()
                        .HasMany(a => a.Characters)
                        .WithMany(b => b.Movies)
                        .OnDelete(DeleteBehavior.ClientSetNull);
            */
        }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

    }
}

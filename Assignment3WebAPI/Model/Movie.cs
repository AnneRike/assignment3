﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3WebAPI.Model
{
    /// <summary>
    /// Model for the table Movie
    /// </summary>
    [Table("Movie")]
    public class Movie
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Title { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Genre { get; set; }

        [MaxLength(4, ErrorMessage = "Do not enter more than 4 characters")]
        public int ReleaseYear { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Director { get; set; }

        [Url(ErrorMessage = "Invalid URL")]
        public string Picture { get; set; }

        [Url(ErrorMessage = "Invalid URL")]
        public string Trailer { get; set; }
        //Foreign key Characters, a movie can have many characters
        public virtual ICollection<Character>? Characters { get; set; }
        //Foreign key Franchise, a movie has one franchise
        public int? FranchiseId { get; set; }
        //public Franchise? Franchise { get; set; }
    }
}

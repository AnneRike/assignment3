﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3WebAPI.Model
{
    /// <summary>
    /// Model for the table character
    /// </summary>
    [Table("Character")]
    public class Character
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string FullName { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Alias { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(10)")]
        public string Gender { get; set; }

        [Url(ErrorMessage = "Invalid URL")]
        public string Picture { get; set; }

        //Foreignkey movies, one character can appear in many movies
        public ICollection<Movie>? Movies { get; set;}
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3WebAPI.Model
{
    /// <summary>
    /// Model for the table Franchise
    /// </summary>
    [Table("Franchise")]
    public class Franchise
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Description { get; set; }

        //Foreignkey for movies in a franchise, one franchise can have many movies
        public ICollection<Movie>? Movies { get; set; }
    }
}

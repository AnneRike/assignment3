﻿using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;
using Assignment3WebAPI.DataTransferObjects.MovieDTO;
using Assignment3WebAPI.Model;
using AutoMapper;

namespace Assignment3WebAPI.Profiles
{
    /// <summary>
    /// Class for mapping the franchise model to Data transfer objects
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            CreateMap<FranchiseCreateDTO, Franchise>();

            CreateMap<FranchiseEditDTO, Franchise>();

            CreateMap<Franchise, FranchiseMoviesDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            CreateMap<Character, FranchiseCharactersDTO>()
                .ForMember(cdto => cdto.Characters, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));
        }
    }
}

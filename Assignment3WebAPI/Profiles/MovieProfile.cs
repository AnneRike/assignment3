﻿using Assignment3WebAPI.Model;
using Assignment3WebAPI.DataTransferObjects.MovieDTO;
using AutoMapper;
using System.Threading.Tasks;
using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;

namespace Assignment3WebAPI.Profiles
{
    /// <summary>
    /// Class for mapping the movie model to Data transfer objects
    /// </summary>
    public class MovieProfile : Profile
    {
        public MovieProfile() 
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()));
            
            CreateMap<MovieCreateDTO, Movie>();
            
            CreateMap<MovieEditDTO, Movie>();

            CreateMap<Movie, MovieCharactersDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()));
        }
    }
}

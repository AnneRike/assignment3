﻿using Assignment3WebAPI.DataTransferObjects.CharacterDTO;
using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;
using Assignment3WebAPI.Model;
using AutoMapper;

namespace Assignment3WebAPI.Profiles
{
    /// <summary>
    /// Class for mapping the character model to Data transfer objects
    /// </summary>
    public class CharacterProfile : Profile
    {

        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterReadDTO, Character>();

            CreateMap<CharacterCreateDTO, Character>();

            CreateMap<CharacterEditDTO, Character>();
        }
    }
}

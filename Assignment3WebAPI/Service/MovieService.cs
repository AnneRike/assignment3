﻿using Assignment3WebAPI.Data;
using Assignment3WebAPI.Model;
using Microsoft.EntityFrameworkCore;

namespace Assignment3WebAPI.Service
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharactersDbContext _context;
        public MovieService(MovieCharactersDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Adds a movie.
        /// </summary>
        /// <param name="movie">A movie object to add.</param>
        /// <returns>The new movie object.</returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Checks if the movie exists in the database.
        /// </summary>
        /// <param name="id">The id of the movie.</param>
        /// <returns>A boolean indicating if a movie with the specified id exists in the database.</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Deletes a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Gets all movies 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Characters)
                .ToListAsync();
        }
        /// <summary>
        /// Gets characters from a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<Movie>> GetSpecificAsync(int id)
        {
            return await _context.Movies
                 .Where(x => x.Id == id)
                 .Select(x => new Movie
                 {
                     Characters = x.Characters
                 })
                 .ToListAsync();

        }
        /// <summary>
        /// Gets one specific movie.
        /// </summary>
        /// <param name="id">Id to check for movie.</param>
        /// <returns>The movie object if found.</returns>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }
        /// <summary>
        /// Updates a movie.
        /// </summary>
        /// <param name="movies">Movie object to update with.</param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movies)
        {
            _context.Entry(movies).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Updates characters in a movie
        /// </summary>
        /// <param name="movieId">movie to update characters in</param>
        /// <param name="characters">character ids to add to movie</param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieCharactersAsync(int movieId, List<int> characters)
        {
            Movie movie = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            List<Character> chars = new();
            foreach (int charId in characters)
            {
                Character character = await _context.Characters.FindAsync(charId);
                if (character == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                chars.Add(character);
            }
            movie.Characters = chars;
            await _context.SaveChangesAsync();
        }

    }
}

﻿using Assignment3WebAPI.Model;

namespace Assignment3WebAPI.Service
{
    /// <summary>
    /// Interface for the MovieService
    /// </summary>
    public interface IMovieService
    {
        public Task UpdateMovieCharactersAsync(int movieId, List<int> certifications);
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task<List<Movie>> GetSpecificAsync(int id);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);

    }
}

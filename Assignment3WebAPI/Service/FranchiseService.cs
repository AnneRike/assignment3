﻿using Assignment3WebAPI.Data;
using Assignment3WebAPI.DataTransferObjects.FranchiseDTO;
using Assignment3WebAPI.Model;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Assignment3WebAPI.Service
{
    /// <summary>
    /// Franchise service for cleaing up the franchisecontroller
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharactersDbContext _context;
        public FranchiseService(MovieCharactersDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Adds a franchise
        /// </summary>
        /// <param name="franchise">A franchise object.</param>
        /// <returns>The new franchise object.</returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        /// <summary>
        /// Checks if a franchise exists
        /// </summary>
        /// <param name="id">Id to check</param>
        /// <returns>A boolean indicating if a franchise with the specified id exists in the database.</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
        /// <summary>
        /// Deletes a franchise
        /// </summary>
        /// <param name="id">Id to delete</param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>IEnumerable with all franchises</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(c => c.Movies)
                .ToListAsync();
        }
        /// <summary>
        /// Gets specific franchise
        /// </summary>
        /// <param name="id">Id for franchise to get</param>
        /// <returns></returns>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }
        /// <summary>
        /// Gets movies in a franchise
        /// </summary>
        /// <param name="id">id of franchise to get movies from</param>
        /// <returns></returns>
        public async Task<List<Franchise>> GetSpecificAsync(int id)
        {
            return await _context.Franchises
                 .Where(x => x.Id == id)
                 .Select(x => new Franchise
                 {
                     Movies = x.Movies
                 })
                 .ToListAsync();
        }
        /// <summary>
        /// Gets characters in movies that are in one franchise
        /// </summary>
        /// <param name="id">Id of franchise to get characters from</param>
        /// <returns></returns>
        public async Task<List<Character>> GetCharactersAsync(int id)
        {
            return await _context.Franchises
                .Where(x => x.Id == id)
                .SelectMany(c => c.Movies)
                .SelectMany(c => c.Characters)
                .Include(a => a.Movies)
                .ToListAsync();
        }
        /// <summary>
        /// Updates a franchise.
        /// </summary>
        /// <param name="franchises">Franchise object to update.</param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchises)
        {
            _context.Entry(franchises).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Updates the the franchise of a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="movi"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public async Task UpdateMovieFranchisesAsync(int movieId, List<int> movi)
        {
            Franchise franchise = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            List<Movie> movies = new();
            foreach (int moviesId in movi)
            {
                Movie movie = await _context.Movies.FindAsync(moviesId);
                if (movie == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }
            franchise.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }
}

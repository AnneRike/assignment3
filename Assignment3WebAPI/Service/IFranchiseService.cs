﻿using Assignment3WebAPI.Model;

namespace Assignment3WebAPI.Service
{
    /// <summary>
    /// Interface for the FranchiseService
    /// </summary>
    public interface IFranchiseService
    {
        public Task UpdateMovieFranchisesAsync(int franchiseId, List<int> movies);
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<List<Franchise>> GetSpecificAsync(int id);

        public Task<List<Character>> GetCharactersAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
